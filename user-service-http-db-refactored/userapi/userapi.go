package userapi

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"
	"user-service/user"
)

type Manager interface {
	All() ([]user.User, error)
	Insert(*user.User) error
	FindByID(id int) (*user.User, error)
	Delete(*user.User) error
	Update(*user.User) error
}
type Handler struct {
	M Manager
}

func (h *Handler) allUserHandler(w http.ResponseWriter, r *http.Request) {
	users, err := h.M.All()
	if err != nil {
		http.Error(w, fmt.Sprintf("users: %s", err), http.StatusInternalServerError)
		return
	}

	byteUsers, err := json.Marshal(users)
	if err != nil {
		http.Error(w, fmt.Sprintf("users: %s", err), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	fmt.Fprintf(w, "%s", byteUsers)
}

func (h *Handler) createUserHanlder(w http.ResponseWriter, r *http.Request) {
	b, err := ioutil.ReadAll(r.Body)
	if err != nil {
		http.Error(w, fmt.Sprintf("users: %s", err), http.StatusInternalServerError)
		return
	}

	var u user.User
	err = json.Unmarshal(b, &u)
	if err != nil {
		http.Error(w, fmt.Sprintf("users: %s", err), http.StatusInternalServerError)
		return
	}

	err = h.M.Insert(&u)
	if err != nil {
		http.Error(w, fmt.Sprintf("users: %s", err), http.StatusInternalServerError)
		return
	}

	byteUser, err := json.Marshal(u)
	if err != nil {
		http.Error(w, fmt.Sprintf("users: %s", err), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusCreated)
	fmt.Fprintf(w, "%s", byteUser)
}

func (h *Handler) getUserHandler(id int, w http.ResponseWriter, r *http.Request) {
	u, err := h.M.FindByID(id)
	if err != nil {
		http.Error(w, fmt.Sprintf("users: %s", err), http.StatusNotFound)
		return
	}

	byteUser, err := json.Marshal(u)
	if err != nil {
		http.Error(w, fmt.Sprintf("users: %s", err), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	fmt.Fprintf(w, "%s", byteUser)
}

func (h *Handler) deleteUserHandler(id int, w http.ResponseWriter, r *http.Request) {
	err := h.M.Delete(&user.User{
		ID: id,
	})
	if err != nil {
		http.Error(w, fmt.Sprintf("users: %s", err), http.StatusInternalServerError)
		return
	}
}

func (h *Handler) updateUserHandler(id int, w http.ResponseWriter, r *http.Request) {
	u, err := h.M.FindByID(id)
	if err != nil {
		http.Error(w, fmt.Sprintf("users: %s", err), http.StatusNotFound)
		return
	}

	b, err := ioutil.ReadAll(r.Body)
	if err != nil {
		http.Error(w, fmt.Sprintf("users: %s", err), http.StatusInternalServerError)
		return
	}

	var update struct {
		FirstName *string `json:"first_name"`
		LastName  *string `json:"last_name"`
		Email     *string `json:"email"`
	}

	err = json.Unmarshal(b, &update)
	if err != nil {
		http.Error(w, fmt.Sprintf("users: %s", err), http.StatusInternalServerError)
		return
	}

	if update.FirstName != nil {
		u.FirstName = *update.FirstName
	}
	if update.LastName != nil {
		u.LastName = *update.LastName
	}
	if update.Email != nil {
		u.Email = *update.Email
	}

	err = h.M.Update(u)
	if err != nil {
		http.Error(w, fmt.Sprintf("users: %s", err), http.StatusInternalServerError)
		return
	}
}

func StartServer(h *Handler) error {
	// http.HandleFunc("/users/", allUserHandler)
	http.Handle("/users/", h.Main())
	return http.ListenAndServe(":8000", nil)
}

func (h *Handler) Main() http.Handler {
	return http.StripPrefix("/users/", http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if r.URL.Path == "" {
			switch r.Method {
			case http.MethodGet:
				h.allUserHandler(w, r)
			case http.MethodPost:
				h.createUserHanlder(w, r)
			default:
				http.NotFound(w, r)
			}
		} else {
			id, err := strconv.Atoi(r.URL.Path)
			if err != nil {
				http.NotFound(w, r)
				return
			}

			switch r.Method {
			case http.MethodGet:
				h.getUserHandler(id, w, r)
			case http.MethodPost, http.MethodPut:
				h.updateUserHandler(id, w, r)
			case http.MethodDelete:
				h.deleteUserHandler(id, w, r)
			}
		}
	}))
}
