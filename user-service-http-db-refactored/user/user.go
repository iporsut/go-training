package user

import (
	"database/sql"
	"encoding/xml"
	"errors"
	"log"

	_ "github.com/lib/pq"
)

type User struct {
	XMLName xml.Name `json:"-" xml:"user"`

	ID        int    `json:"id" xml:"id,attr"`
	FirstName string `json:"first_name" xml:"first-name"`
	LastName  string `json:"last_name" xml:"last-name"`
	Email     string `json:"email" xml:"email"`
}

type Manager struct {
	DB *sql.DB
}

func (m *Manager) Insert(user *User) error {
	r := m.DB.QueryRow("INSERT INTO users(first_name, last_name, email) VALUES ($1,$2,$3) RETURNING id", user.FirstName, user.LastName, user.Email)
	err := r.Scan(&user.ID)
	if err != nil {
		return err
	}
	return nil
}

func (m *Manager) Update(user *User) error {
	_, err := m.DB.Exec("UPDATE users SET first_name = $1, last_name = $2, email = $3 WHERE id = $4", user.FirstName, user.LastName, user.Email, user.ID)
	if err != nil {
		return err
	}
	return nil
}

func (m *Manager) FindByID(id int) (*User, error) {
	row := m.DB.QueryRow("SELECT id, first_name, last_name, email FROM users WHERE id = $1", id)
	var u User
	err := row.Scan(&u.ID, &u.FirstName, &u.LastName, &u.Email)
	if err != nil {
		return nil, err
	}
	return &u, nil
}

func (m *Manager) Delete(user *User) error {
	r, err := m.DB.Exec("DELETE FROM users WHERE id = $1", user.ID)
	effect, err := r.RowsAffected()
	if err != nil {
		return err
	}
	if effect != 1 {
		return errors.New("user: delete not have effected row")
	}
	return nil
}

func (m *Manager) All() ([]User, error) {
	users := []User{}
	rows, err := m.DB.Query("SELECT id, first_name, last_name, email FROM users")
	if err != nil {
		return nil, err
	}
	for rows.Next() {
		var u User
		err := rows.Scan(&u.ID, &u.FirstName, &u.LastName, &u.Email)
		if err != nil {
			return nil, err
		}
		users = append(users, u)
	}
	return users, nil
}

func (m *Manager) ResetStorage() {
	_, err := m.DB.Exec("TRUNCATE TABLE users RESTART IDENTITY;")
	if err != nil {
		log.Fatal(err)
	}
}

func (m *Manager) Last() User {
	var u User
	row := m.DB.QueryRow("SELECT id, first_name, last_name, email FROM users ORDER BY id DESC LIMIT 1")
	err := row.Scan(&u.ID, &u.FirstName, &u.LastName, &u.Email)
	if err != nil {
		log.Fatal(err)
	}
	return u
}
