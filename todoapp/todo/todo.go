package todo

import "fmt"

// Todo store item todo
type Todo struct {
	ID   int
	Body string
}

func (t *Todo) String() string {
	return fmt.Sprintf("ID: %d => %s", t.ID, t.Body)
}

var idSeq int

// NewTodo make Todo with auto increment ID
func New(body string) *Todo {
	todo := &Todo{
		ID:   idSeq,
		Body: body,
	}
	idSeq++

	return todo
}
