# Account Service Exercise
```
GET /accounts/
RESP BODY
[
        {
                "id": 1,
                "name": "John Doe",
                "balance": 10000,
        }
]

POST /accounts/
RESQ BODY
{
                "name": "John Doe",
}
RESP BODY
{
        "id": 1,
        "name": "John Doe",
        "balance": 0,
}

GET /acounts/1
RESP BODY
{
        "id": 1,
        "name": "John Doe",
        "balance": 0,
}

POST, PUT /accounts/1
RESQ BODY
{
        "name": "Jan Dee",
}

DELETE /accounts/1

POST /accounts/1/deposit
RESQ BODY
{
        "amount": 100
}

POST /accounts/1/withdraw
RESQ BODY
{
        "amount": 100
}
```
