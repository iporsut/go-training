package main

import (
	"fmt"
	"sync"
)

func gen(wg *sync.WaitGroup) chan int {
	out := make(chan int)
	go func() {
		for i := 1; i <= 100; i++ {
			out <- i
		}
		wg.Done()
	}()
	return out
}

func genEven(wg *sync.WaitGroup) chan int {
	out := make(chan int)
	go func() {
		for i := 0; i <= 200; i += 2 {
			out <- i
		}
		wg.Done()
	}()
	return out
}

func main() {
	var wg sync.WaitGroup
	wg.Add(2)
	out := gen(&wg)
	outEven := genEven(&wg)
	done := make(chan struct{})
	go func() {
		wg.Wait()
		close(done)
	}()
	for {
		select {
		case v := <-out:
			fmt.Println(v)
		case v := <-outEven:
			fmt.Println("Even:", v)
		case <-done:
			return
		}
	}
}
