package userapi_test

import (
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
	"time"
	"user-service/user"
	"user-service/userapi"

	"github.com/stretchr/testify/assert"
)

func TestAllUserAPI(t *testing.T) {
	defer user.ResetStorage()
	user.Insert(&user.User{
		FirstName: "Weerasak",
		LastName:  "Chongnguluam",
		Email:     "singpor@gmail.com",
	})
	user.Insert(&user.User{
		FirstName: "Kanokorn",
		LastName:  "Chongnguluam",
		Email:     "kanokorn@gmail.com",
	})

	ts := httptest.NewServer(userapi.UserHandler)
	defer ts.Close()

	resp, err := http.Get(ts.URL + "/users/")
	assert.NoError(t, err)
	b, err := ioutil.ReadAll(resp.Body)
	defer resp.Body.Close()
	assert.NoError(t, err)
	assert.Equal(t, http.StatusText(http.StatusOK), http.StatusText(resp.StatusCode))
	assert.Equal(t, "application/json", resp.Header.Get("Content-Type"))
	expectJSON := `[{"id":1,"first_name":"Weerasak","last_name":"Chongnguluam","email":"singpor@gmail.com"},{"id":2,"first_name":"Kanokorn","last_name":"Chongnguluam","email":"kanokorn@gmail.com"}]`
	assert.Equal(t, expectJSON, string(b))
}

func TestGetUserAPI(t *testing.T) {
	defer user.ResetStorage()
	user.Insert(&user.User{
		FirstName: "Weerasak",
		LastName:  "Chongnguluam",
		Email:     "singpor@gmail.com",
	})
	user.Insert(&user.User{
		FirstName: "Kanokorn",
		LastName:  "Chongnguluam",
		Email:     "kanokorn@gmail.com",
	})

	ts := httptest.NewServer(userapi.UserHandler)
	defer ts.Close()

	resp, err := http.Get(ts.URL + "/users/1")
	defer resp.Body.Close()
	assert.NoError(t, err)
	b, _ := ioutil.ReadAll(resp.Body)

	assert.Equal(t, http.StatusText(http.StatusOK), http.StatusText(resp.StatusCode))
	assert.Equal(t, "application/json", resp.Header.Get("Content-Type"))
	expectJSON := `{"id":1,"first_name":"Weerasak","last_name":"Chongnguluam","email":"singpor@gmail.com"}`
	assert.Equal(t, expectJSON, string(b))
}

func TestDeleteUserAPI(t *testing.T) {
	defer user.ResetStorage()
	user.Insert(&user.User{
		FirstName: "Weerasak",
		LastName:  "Chongnguluam",
		Email:     "singpor@gmail.com",
	})
	user.Insert(&user.User{
		FirstName: "Kanokorn",
		LastName:  "Chongnguluam",
		Email:     "kanokorn@gmail.com",
	})

	ts := httptest.NewServer(userapi.UserHandler)
	defer ts.Close()

	r, _ := http.NewRequest(http.MethodDelete, ts.URL+"/users/1", nil)
	resp, err := http.DefaultClient.Do(r)
	assert.NoError(t, err)
	defer resp.Body.Close()
	assert.Equal(t, http.StatusText(http.StatusOK), http.StatusText(resp.StatusCode))

	r, _ = http.NewRequest(http.MethodDelete, ts.URL+"/users/3", nil)
	r.Header.Set("Content-Type", "application/json")
	resp, err = http.DefaultClient.Do(r)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusText(http.StatusInternalServerError), http.StatusText(resp.StatusCode))
}

func TestCreateUserAPI(t *testing.T) {
	defer user.ResetStorage()

	ts := httptest.NewServer(userapi.UserHandler)
	defer ts.Close()

	jsonBody := `{"first_name":"Weerasak","last_name":"Chongnguluam","email":"singpor@gmail.com"}`
	resp, err := http.Post(ts.URL+"/users/", "application/json", strings.NewReader(jsonBody))
	assert.NoError(t, err)
	b, err := ioutil.ReadAll(resp.Body)
	defer resp.Body.Close()
	assert.NoError(t, err)
	assert.Equal(t, http.StatusText(http.StatusCreated), http.StatusText(resp.StatusCode))
	assert.Equal(t, "application/json", resp.Header.Get("Content-Type"))
	expectJSON := `{"id":1,"first_name":"Weerasak","last_name":"Chongnguluam","email":"singpor@gmail.com"}`
	assert.Equal(t, expectJSON, string(b))
}

func TestUpdateUserAPI(t *testing.T) {
	defer user.ResetStorage()
	user.Insert(&user.User{
		FirstName: "Weerasak",
		LastName:  "Chongnguluam",
		Email:     "singpor@gmail.com",
	})

	ts := httptest.NewServer(userapi.UserHandler)
	defer ts.Close()

	jsonBody := `{"email":"singpor@hotmail.com"}`
	r, _ := http.NewRequest(http.MethodPut, ts.URL+"/users/1", strings.NewReader(jsonBody))
	r.Header.Set("Content-Type", "application/json")
	resp, err := http.DefaultClient.Do(r)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusText(http.StatusOK), http.StatusText(resp.StatusCode))
	u, _ := user.FindByID(1)
	assert.Equal(t, &user.User{
		ID:        1,
		FirstName: "Weerasak",
		LastName:  "Chongnguluam",
		Email:     "singpor@hotmail.com",
	}, u)
}

func TestIntegration(t *testing.T) {
	go func() {
		userapi.StartServer()
	}()
	time.Sleep(100 * time.Millisecond)
	resp, err := http.Get("http://localhost:8000/users/")
	assert.NoError(t, err)
	assert.Equal(t, http.StatusOK, resp.StatusCode)
}
