package user

import (
	"errors"
	"sort"
	"sync"
)

var ErrNotFound = errors.New("user: not found")

var (
	mu          sync.Mutex
	userStorage = make(map[int]User)
	idSeq       int
)

type User struct {
	ID        int    `json:"id"`
	FirstName string `json:"first_name"`
	LastName  string `json:"last_name"`
	Email     string `json:"email"`
}

func Insert(user *User) error {
	mu.Lock()
	defer mu.Unlock()

	idSeq++
	user.ID = idSeq
	userStorage[idSeq] = *user
	return nil
}

func Update(user *User) error {
	mu.Lock()
	defer mu.Unlock()

	if _, ok := userStorage[user.ID]; !ok {
		return ErrNotFound
	}
	userStorage[user.ID] = *user
	return nil
}

func Delete(user *User) error {
	mu.Lock()
	defer mu.Unlock()

	if _, ok := userStorage[user.ID]; !ok {
		return ErrNotFound
	}
	delete(userStorage, user.ID)
	return nil
}

func FindByID(id int) (*User, error) {
	mu.Lock()
	defer mu.Unlock()

	if _, ok := userStorage[id]; !ok {
		return nil, ErrNotFound
	}
	user := userStorage[id]
	return &user, nil
}

func All() ([]User, error) {
	mu.Lock()
	defer mu.Unlock()

	users := make([]User, 0, len(userStorage))
	for _, v := range userStorage {
		users = append(users, v)
	}
	sort.Slice(users, func(i, j int) bool { return users[i].ID < users[j].ID })
	return users, nil
}

func Last() User {
	mu.Lock()
	defer mu.Unlock()

	return userStorage[idSeq]
}

func ResetStorage() {
	mu.Lock()
	defer mu.Unlock()

	idSeq = 0
	userStorage = make(map[int]User)
}
