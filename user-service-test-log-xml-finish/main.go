package main

import (
	"log"
	"user-service/user"
	"user-service/userapi"
)

func main() {
	user.Insert(&user.User{
		FirstName: "Weerasak",
		LastName:  "Chongnguluam",
		Email:     "singpor@gmail.com",
	})
	user.Insert(&user.User{
		FirstName: "Kanokorn",
		LastName:  "Chongnguluam",
		Email:     "kanokorn@gmail.com",
	})

	log.Fatal(userapi.StartServer())
}
