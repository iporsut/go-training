package userapi

import (
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
	"user-service/user"

	"github.com/stretchr/testify/assert"
)

func TestAllUserHandler(t *testing.T) {
	defer user.ResetStorage()
	user.Insert(&user.User{
		FirstName: "Weerasak",
		LastName:  "Chongnguluam",
		Email:     "singpor@gmail.com",
	})
	user.Insert(&user.User{
		FirstName: "Kanokorn",
		LastName:  "Chongnguluam",
		Email:     "kanokorn@gmail.com",
	})

	r := httptest.NewRequest("GET", "http://lvm.me:8000/users/", nil)
	w := httptest.NewRecorder()
	allUserHandler(w, r)
	resp := w.Result()
	body, err := ioutil.ReadAll(resp.Body)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusText(http.StatusOK), http.StatusText(resp.StatusCode))
	assert.Equal(t, "application/json", resp.Header.Get("Content-Type"))
	expectJSON := `[{"id":1,"first_name":"Weerasak","last_name":"Chongnguluam","email":"singpor@gmail.com"},{"id":2,"first_name":"Kanokorn","last_name":"Chongnguluam","email":"kanokorn@gmail.com"}]`
	assert.Equal(t, expectJSON, string(body))
}

func TestGetUserHandler(t *testing.T) {
	defer user.ResetStorage()
	user.Insert(&user.User{
		FirstName: "Weerasak",
		LastName:  "Chongnguluam",
		Email:     "singpor@gmail.com",
	})
	user.Insert(&user.User{
		FirstName: "Kanokorn",
		LastName:  "Chongnguluam",
		Email:     "kanokorn@gmail.com",
	})

	r := httptest.NewRequest("GET", "http://lvm.me:8000/users/1", nil)
	w := httptest.NewRecorder()
	getUserHandler(1, w, r)
	resp := w.Result()
	body, err := ioutil.ReadAll(resp.Body)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusText(http.StatusOK), http.StatusText(resp.StatusCode))
	assert.Equal(t, "application/json", resp.Header.Get("Content-Type"))
	expectJSON := `{"id":1,"first_name":"Weerasak","last_name":"Chongnguluam","email":"singpor@gmail.com"}`
	assert.Equal(t, expectJSON, string(body))
}

func TestDeleteUserHandler(t *testing.T) {
	// Excercise
}

func TestCreateUserHandler(t *testing.T) {
	defer user.ResetStorage()
	jsonBody := `{"first_name":"Weerasak","last_name":"Chongnguluam","email":"singpor@gmail.com"}`
	r := httptest.NewRequest("POST", "http://lvm.me:8000/users/", strings.NewReader(jsonBody))
	w := httptest.NewRecorder()
	createUserHanlder(w, r)
	resp := w.Result()
	body, err := ioutil.ReadAll(resp.Body)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusText(http.StatusCreated), http.StatusText(resp.StatusCode))
	assert.Equal(t, "application/json", resp.Header.Get("Content-Type"))
	expectJSON := `{"id":1,"first_name":"Weerasak","last_name":"Chongnguluam","email":"singpor@gmail.com"}`
	assert.Equal(t, expectJSON, string(body))
}

func TestUpdateUserHandler(t *testing.T) {
	// Excercise
}
