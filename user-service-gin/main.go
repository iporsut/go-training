package main

import (
	"database/sql"
	"log"
	"os"
	"user-service-gin/userapi"

	_ "github.com/lib/pq"
)

func main() {
	db, err := sql.Open("postgres", os.Getenv("DATABASE_URL"))
	if err != nil {
		log.Fatal(err)
	}

	userapi.StartServer(":"+os.Getenv("PORT"), db)
}
