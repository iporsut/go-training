module learn-sql

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/gin-contrib/sse v0.0.0-20170109093832-22d885f9ecc7 // indirect
	github.com/gin-gonic/gin v1.3.0 // indirect
	github.com/golang/protobuf v1.2.0 // indirect
	github.com/gorilla/mux v1.6.2
	github.com/heroku/go-getting-started v0.0.0-20171212200808-a8bffc1cac53 // indirect
	github.com/heroku/x v0.0.0-20181102215100-85e5aa5e6aa1 // indirect
	github.com/lib/pq v1.0.0
	github.com/mattn/go-isatty v0.0.4 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/stretchr/testify v1.2.2
	github.com/ugorji/go/codec v0.0.0-20181119220752-0165389f8c91 // indirect
	gopkg.in/go-playground/validator.v8 v8.18.2 // indirect
	gopkg.in/yaml.v2 v2.2.1 // indirect
)
