package main

import (
	"database/sql"
	"flag"
	"fmt"
	"learn-sql/userapi"
	"log"

	_ "github.com/lib/pq"
)

func main() {
	host := flag.String("host", "localhost", "Host")
	port := flag.String("port", "8000", "Port")
	dbURL := flag.String("dburl", "", "DB Connection")
	flag.Parse()

	addr := fmt.Sprintf("%s:%s", *host, *port)
	// connStr := "postgres://tpxbvdny:3xQCeyAUtP6Hq5uqgPCTrI54cfRUqzTe@elmer.db.elephantsql.com:5432/tpxbvdny"
	db, err := sql.Open("postgres", *dbURL)
	if err != nil {
		log.Fatal(err)
	}

	log.Fatal(userapi.StartServer(addr, db))
}
