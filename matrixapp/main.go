package main

import (
	"fmt"
	"matrixapp/matrix"
)

func main() {
	a := [][]float64{{1, 2}, {3, 4}}
	b := [][]float64{{5, 6}, {7, 8}}
	fmt.Println(matrix.Sum(a, b))
}
