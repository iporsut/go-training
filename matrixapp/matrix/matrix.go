package matrix

func Sum(a, b [][]float64) (c [][]float64) {
	for i := range a {
		var sumRow []float64
		for j := range a[i] {
			sumRow = append(sumRow, a[i][j]+b[i][j])
		}
		c = append(c, sumRow)
	}
	return
}
