package user

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestInsert(t *testing.T) {
	ConnectDB()
	defer ResetStorage()

	u := User{
		FirstName: "Weerasak",
		LastName:  "Chongnguluam",
		Email:     "singpor@gmail.com",
	}
	err := Insert(&u)
	assert.NoError(t, err)
	assert.Equal(t, u, Last())

	u = User{
		FirstName: "Kanokorn",
		LastName:  "Chongnguluam",
		Email:     "kanokorn@gmail.com",
	}
	err = Insert(&u)
	assert.NoError(t, err)
	assert.Equal(t, u, Last())
}

func TestUpdate(t *testing.T) {
	ConnectDB()
	defer ResetStorage()

	u := User{
		FirstName: "Weerasak",
		LastName:  "Chongnguluam",
		Email:     "singpor@gmail.com",
	}
	err := Insert(&u)
	assert.NoError(t, err)

	u = Last()
	u.Email = "singpor@outlook.com"
	err = Update(&u)
	assert.NoError(t, err)
	assert.Equal(t, u, Last())
}

func TestFindByID(t *testing.T) {
	ConnectDB()
	defer ResetStorage()

	u := User{
		FirstName: "Weerasak",
		LastName:  "Chongnguluam",
		Email:     "singpor@gmail.com",
	}
	err := Insert(&u)
	assert.NoError(t, err)

	r, err := FindByID(1)
	assert.NoError(t, err)
	assert.Equal(t, u, *r)

	r, err = FindByID(2)
	assert.Nil(t, r)
	assert.Error(t, err)
}

func TestDelete(t *testing.T) {
	ConnectDB()
	defer ResetStorage()

	u := User{
		FirstName: "Weerasak",
		LastName:  "Chongnguluam",
		Email:     "singpor@gmail.com",
	}
	err := Insert(&u)
	assert.NoError(t, err)

	r, err := FindByID(1)
	assert.NoError(t, err)

	err = Delete(r)
	assert.NoError(t, err)

	err = Delete(r)
	assert.Error(t, err)
}

func TestAll(t *testing.T) {
	ConnectDB()
	defer ResetStorage()

	Insert(&User{
		FirstName: "Weerasak",
		LastName:  "Chongnguluam",
		Email:     "singpor@gmail.com",
	})
	Insert(&User{
		FirstName: "Kanokorn",
		LastName:  "Chongnguluam",
		Email:     "kanokorn@gmail.com",
	})

	users, err := All()
	assert.NoError(t, err)
	assert.Equal(t, []User{
		{
			ID:        1,
			FirstName: "Weerasak",
			LastName:  "Chongnguluam",
			Email:     "singpor@gmail.com",
		},
		{
			ID:        2,
			FirstName: "Kanokorn",
			LastName:  "Chongnguluam",
			Email:     "kanokorn@gmail.com",
		},
	}, users)
}
