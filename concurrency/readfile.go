package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"sync"
)

func printLine(filename string, wg *sync.WaitGroup) {
	f, err := os.Open(filename)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		fmt.Println(scanner.Text())
	}
	wg.Done()
}

func main() {
	var wg sync.WaitGroup
	wg.Add(2)
	go printLine("input.txt", &wg)
	go printLine("input2.txt", &wg)
	wg.Wait()
}
