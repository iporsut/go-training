package main

import "fmt"

func genInt(begin, end int) chan int {
	out := make(chan int, end-begin+1)
	for i := begin; i <= end; i++ {
		out <- i
	}
	close(out)
	return out
}

func main() {
	out := genInt(1, 100)
	for v := range out {
		fmt.Println(v)
	}
}
